/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "traffic_light_component.h"

#include <experimental/filesystem>

#include "base/common/log.h"
#include "base/device_connect/traffic_light/device_factory.h"
#include "base/plugin/modules_loader/dynamic_loader.h"

namespace os {
namespace v2x {
namespace device {

bool AIROS_COMPONENT_CLASS_NAME(TrafficLightComponent)::Init() {
  airos::base::DynamicLoader::GetInstance().LoadDevicePackage(
      "device/lib/traffic_light/");
  if (!LoadConfig(&conf_)) {
    LOG_ERROR << "load component proto config error";
    return false;
  }

  device_ = TrafficLightDeviceFactory::Instance().GetUnique(
      conf_.device(),
      std::bind(
          &AIROS_COMPONENT_CLASS_NAME(TrafficLightComponent)::CallBack,
          this,
          std::placeholders::_1));

  // 调用厂商的init方法，连接设备
  if (device_ == nullptr || !device_->Init(conf_.config_file())) {
    LOG_ERROR << "device_ init error";
    return false;
  }

  // 启动设备处理pipline，向node_发送处理后的proto
  task_.reset(new std::thread([&]() {
    device_->Start();
  }));
  return true;
}

// Proc方法：用于解析接收到的proto，并写入设备
bool AIROS_COMPONENT_CLASS_NAME(TrafficLightComponent)::Proc(
    const std::shared_ptr<const os::v2x::device::TrafficLightReceiveData>&
        recv_data) {
  device_->WriteToDevice(recv_data);
  return true;
}

void AIROS_COMPONENT_CLASS_NAME(TrafficLightComponent)::CallBack(
    const TrafficLightDataType& data) {
  Send("/airos/device/traffic_light/data", data);
}

}  // namespace device
}  // namespace v2x
}  // namespace os
