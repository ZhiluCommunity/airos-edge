/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "air_middleware_common.h"

namespace airos {
namespace middleware {

class AirMiddlewareRate {
 public:
  explicit AirMiddlewareRate(double frequency) {
    rate_impl_.reset(new RATE_IMPL(frequency));
  }
  explicit AirMiddlewareRate(uint64_t nanoseconds) {
    rate_impl_.reset(new RATE_IMPL(nanoseconds));
  }

  void Sleep() {
    rate_impl_->Sleep();
  }

 private:
  std::shared_ptr<RATE_IMPL> rate_impl_;
};

}  // namespace middleware
}  // namespace airos
