/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "v2x_codec_component.h"

#include <sys/time.h>

#include <gflags/gflags.h>

#include "base/common/log.h"

namespace os {
namespace v2x {
namespace protocol {

DEFINE_string(asn_version, "4layer", "v2x message Asn.1 file version");

bool CODEC_COMPONENT::Init() {
  std::string asn_version(FLAGS_asn_version);
  if (asn_version.compare("4layer") == 0) {
    asn_type_ = EnAsnType::CASE_53_2020;
  } else if (asn_version.compare("new_4layer") == 0) {
    asn_type_ = EnAsnType::YDT_3709_2020;
  } else {
    // pass
  }
  // parser
  auto reader = node_->CreateReader<os::v2x::device::RSUData>(
      "/airos/device/rsu_out",
      std::bind(&CODEC_COMPONENT::RSUMessageProc, this, std::placeholders::_1));
  if (reader == nullptr) {
    return false;
  }
  return true;
}

bool CODEC_COMPONENT::RsuPb2MessageFrame(
    const std::shared_ptr<const os::v2x::device::RSUData>& rsu_pb_data,
    std::shared_ptr<v2xpb::asn::MessageFrame>& frame_pb) {
  if (!rsu_pb_data || !frame_pb || !rsu_pb_data->has_data()) {
    LOG_INFO << "input data error";
    return false;
  }

  if (rsu_pb_data->has_type()) {
    LOG_WARN << "recv rsu data type: " << RSUDataType_Name(rsu_pb_data->type());
  }

  if (rsu_pb_data->has_version()) {
    switch (rsu_pb_data->version()) {
      case os::v2x::device::CSAE_53_2020:
        asn_type_ = EnAsnType::CASE_53_2020;
        break;
      case os::v2x::device::YDT_3709_2020:
        asn_type_ = EnAsnType::YDT_3709_2020;
        break;
      default:
        LOG_WARN << "rsu data version:false";
        return false;
    }
  } else {
    LOG_WARN << "rsu data version null";
  }

  std::string str_asn(
      rsu_pb_data->data().c_str(), rsu_pb_data->data().length());
  std::string str_pb("");
  if (0 > message_frame_uper2pbstr_adapter(str_asn, &str_pb, asn_type_)) {
    LOG_WARN << "asn to pb false";
    return false;
  }
  LOG_INFO << "asn to pb:true";

  if (!frame_pb->ParsePartialFromString(str_pb)) {
    LOG_WARN << "pb parse:false";
    return false;
  }

  return true;
}

void CODEC_COMPONENT::RSUMessageProc(
    const std::shared_ptr<const os::v2x::device::RSUData>& rsu_pb_data) {
  if (!rsu_pb_data) {
    LOG_WARN << "ptr null";
    return;
  }
  std::shared_ptr<v2xpb::asn::MessageFrame> frame_pb =
      std::make_shared<v2xpb::asn::MessageFrame>();
  if (!RsuPb2MessageFrame(rsu_pb_data, frame_pb)) {
    LOG_WARN << " recvpb to sendpb false";
    return;
  }

  LOG_INFO << "write pb:" << frame_pb->DebugString();
  Send("/airos/message/received", frame_pb);
  return;
}

bool CODEC_COMPONENT::Proc(
    const std::shared_ptr<const v2xpb::asn::MessageFrame>& frame) {
  if (!frame) {
    LOG_WARN << "frame is nullptr";
    return false;
  }
  auto encode_pb = std::make_shared<os::v2x::device::RSUData>();
  if (!MessageFrame2RsuPb(frame, encode_pb)) {
    LOG_WARN << "message frame to rsupb failed";
    return false;
  }
  Send("/airos/device/rsu_in", encode_pb);
  return true;
}

bool CODEC_COMPONENT::MessageFrame2RsuPb(
    const std::shared_ptr<const v2xpb::asn::MessageFrame>& frame,
    std::shared_ptr<os::v2x::device::RSUData> encode_pb) {
  if (!frame || !encode_pb) {
    LOG_WARN << "input is nullptr";
    return false;
  }

  os::v2x::device::RSUDataType data_type;
  switch (frame->payload_case()) {
    case v2xpb::asn::MessageFrame::PayloadCase::kBsmFrame:
      data_type = os::v2x::device::RSU_BSM;
      LOG_INFO << "recv msg pb bsm";
      break;
    case v2xpb::asn::MessageFrame::PayloadCase::kMapFrame:
      data_type = os::v2x::device::RSU_MAP;
      LOG_INFO << "recv msg pb map";
      break;
    case v2xpb::asn::MessageFrame::PayloadCase::kRsmFrame:
      data_type = os::v2x::device::RSU_RSM;
      LOG_INFO << "recv msg pb rsm";
      break;
    case v2xpb::asn::MessageFrame::PayloadCase::kSpatFrame:
      data_type = os::v2x::device::RSU_SPAT;
      LOG_INFO << "recv msg pb spat";
      break;
    case v2xpb::asn::MessageFrame::PayloadCase::kRsiFrame:
      data_type = os::v2x::device::RSU_RSI;
      LOG_INFO << "recv msg pb rsi";
      break;
    default:
      LOG_WARN << "msg type not support ";
      return false;
  }

  std::string str_pb;
  if (!frame->SerializePartialToString(&str_pb)) {
    LOG_WARN << "pb serialize false";
    return false;
  }

  std::string str_asn;
  if (0 > message_frame_pbstr2uper_adapter(str_pb, &str_asn, asn_type_)) {
    LOG_WARN << "pb to asn false";
    return false;
  }

  if (asn_type_ == EnAsnType::YDT_3709_2020) {
    encode_pb->set_version(os::v2x::device::YDT_3709_2020);
  } else {
    encode_pb->set_version(os::v2x::device::CSAE_53_2020);
  }

  struct timeval tv;
  gettimeofday(&tv, NULL);
  encode_pb->set_time_stamp(
      static_cast<uint64_t>(tv.tv_sec * 1000 + tv.tv_usec / 1000));
  encode_pb->set_data(
      (const void*)(str_asn.data()), static_cast<size_t>(str_asn.size()));
  encode_pb->set_type(data_type);

  return true;
}

}  // namespace protocol
}  // namespace v2x
}  // namespace os
