#!/usr/bin/env bash
set -e

log_file=/home/airos/log/airos_launch.log."$(date +%Y%m%d-%H%M%S)"

return_info() {
  local DATE_N=$(date "+%Y-%m-%d %H:%M:%S")
  [[ $# -eq 0 ]] && return
  echo -e "\033[1;32m [INFO]--- ${DATE_N} $* \033[0m" 2>&1 | tee -a "${log_file}"
}

function check_run() {
  local command_line="$1"
  if [[ -n ${command_line} ]]; then
    launch_cmd="${command_line} > ${log_file} 2>&1 &"
    eval ${launch_cmd}
    return_info "${launch_cmd} , run successfully! "
  fi
}

function start_camera_viz() {
    mainboard -d dag/perception_camera_123_viz.dag
}

function start_fusion_viz() {
    mainboard -d dag/msf_component_viz.dag
}

function start_perception_pipeline() {
    check_run "mainboard -d dag/perception_camera_123.dag"
    check_run "mainboard -d dag/msf_component.dag"
}

function start_perception_lidar() {
    check_run "mainboard -d dag/perception_lidar_12.dag"
}

function start_traffic_light_pipeline() {
    check_run "mainboard -d dag/traffic_light_service.dag"
    check_run "mainboard -d dag/v2x_codec.dag"
    check_run "mainboard -d dag/rsu_service.dag"
    check_run "./bin/airos_app_framework conf/airos_v2x_app.pb"
}

function start_all() {
    check_run "mainboard -d dag/traffic_light_service.dag"
    check_run "mainboard -d dag/v2x_codec.dag"
    check_run "mainboard -d dag/rsu_service.dag"
    check_run "mainboard -d dag/perception_camera_123.dag"
    check_run "mainboard -d dag/msf_component.dag"
    check_run "./bin/airos_app_framework conf/airos_v2x_app.pb"
}

function stop_all() {
    pkill -f mainboard || true
    pkill -f airos_app_framework || true
}

function print_usage() {
  echo "Usage: airos_launch [OPTIONS]"
  echo "Description: This is a tool script for launch airos pipeline."
  echo
  echo "Options:"
  echo "  -h              Display this help message."
  echo "  camera_viz      Start perception camera viz."
  echo "  fusion_viz      Start fusion viz."
  echo "  perception      Start perception pipeline."
  echo "  traffic_light   Start traffic light pipeline."
  echo "  lidar           Start lidar perception."
  echo "  all             Start airos all pipeline."
  echo "  stop            Stop all pipeline."
  echo
}

function main() {
    if [ "$#" -eq 0 ]; then
        print_usage
        exit 0;
    fi

    stop_all
    cd /home/airos/os
    source setup.bash

    local cmd="$1"
    shift
    case "${cmd}" in
        camera_viz)
            start_camera_viz
            ;;
        fusion_viz)
            start_fusion_viz
            ;;
        perception)
            start_perception_pipeline
            ;;
        traffic_light)
            start_traffic_light_pipeline
            ;;
        lidar)
            start_perception_lidar
            ;;
        all)
            start_all
            ;;
        stop)
            echo "all stop!"
            ;;
        *)
            print_usage
            ;;
    esac
}

main "$@"