#! /usr/bin/env bash
set -e

TOP_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)" # 得到当前脚本执行的目录

ARCH="$(uname -m)"
SUPPORTED_ARCHS=" x86_64 aarch64 "

SUPPORTED_DEVICE_SERVICE=(traffic_light rsu camera lidar radar)

RELEASE_ROOT="/home/airos"
OUT_ROOT="${RELEASE_ROOT}/os"
OUT_3RD="${OUT_ROOT}/3rd"
OUT_DAG="${OUT_ROOT}/dag"
OUT_CONF="${OUT_ROOT}/conf"
OUT_LIB="${OUT_ROOT}/lib"
OUT_BIN="${OUT_ROOT}/bin"

device_service_path="middleware/device_service/framework"
device_modules_path="middleware/device_service/modules"

function check_architecture_support() {
    if [[ "${SUPPORTED_ARCHS}" != *" ${ARCH} "* ]]; then
        echo "Unsupported CPU arch: ${ARCH}. Currently, AIROS only" \
            "supports running on the following CPU archs:"
        echo "${TAB}${SUPPORTED_ARCHS}"
        exit 1
    fi
}

function check_platform_support() {
    local platform="$(uname -s)"
    if [[ "${platform}" != "Linux" ]]; then
        echo "Unsupported platform: ${platform}."
        echo "${TAB}AIROS is expected to run on Linux systems (E.g., Debian/Ubuntu)."
        exit 1
    fi
}

function check_minimal_memory_requirement() {
    local minimal_mem_gb="2.0"
    local actual_mem_gb="$(free -m | awk '/Mem:/ {printf("%0.2f", $2 / 1024.0)}')"
    if (($(echo "$actual_mem_gb < $minimal_mem_gb" | bc -l))); then
        echo "System memory [${actual_mem_gb}G] is lower than the minimum required" \
            "[${minimal_mem_gb}G]. AIROS build could fail."
    fi
}

function env_setup() {
    check_architecture_support
    check_platform_support
    check_minimal_memory_requirement
}

function run_base_device_connect_ut() {
    local support_device=(rsu traffic_light ins radar lidar)
    for device in ${support_device[@]}
    do
        pushd ./bazel-bin/${device_modules_path}/${device} > /dev/null
            ./${device}_ut
        popd > /dev/null
    done
    echo "base device connect ut done!"
}

function release_middleware_device_service() {
    for device in ${SUPPORTED_DEVICE_SERVICE[@]}
    do
        local so_path="${TOP_DIR}/bazel-bin/${device_service_path}/${device}/lib${device}_component.so"
        if [ -e "${so_path}" ];then
            cp -f ${so_path} ${OUT_LIB}
        fi
        local dag_path="${TOP_DIR}/${device_service_path}/${device}/dag"
        if [ -d "${dag_path}" ];then
            cp -rf ${dag_path}/* ${OUT_DAG}
        fi

        # framework conf
        device_framework_cfg_path=${OUT_CONF}/${device}
        [ -d "${device_framework_cfg_path}" ] || mkdir -p ${device_framework_cfg_path}
        if [ "${device}" = "camera" ] || [ "${device}" = "lidar" ]; then
          if [ ! -e "${OUT_CONF}/${device}/${device}.yaml" ]; then
            ln -s ${RELEASE_ROOT}/param/device/${device}/${device}.yaml ${OUT_CONF}/${device}/${device}.yaml
          fi   
        else
          local conf_path="${TOP_DIR}/${device_service_path}/${device}/conf/config.pb.txt"
          if [ -e "${conf_path}" ];then
              cp -rf ${conf_path} ${device_framework_cfg_path}
          fi
        fi

    done
}

function release_middleware_protocol(){
    local codec_path="middleware/protocol/v2x_codec"
    local so_path="${TOP_DIR}/bazel-bin/${codec_path}/libv2x_codec.so"
    if [ -e "${so_path}" ];then
        cp -f ${so_path} ${OUT_LIB}
    fi

    local conf_path="${TOP_DIR}/${codec_path}/conf/v2x_codec.flag"
    cp -f ${conf_path} ${OUT_CONF}

    local dag_path="${TOP_DIR}/${codec_path}/dag"
    if [ -d "${dag_path}" ];then
        cp -rf ${dag_path}/* ${OUT_DAG}
    fi
}

function copy_lib_from_dir() {
   for file in ` ls $1`
   do
       if [ -d $1"/"$file ]
       then
            if [ "${file##*.}"x != "runfiles"x ]
            then
                copy_lib_from_dir $1"/"$file $2
            fi
       else
            if [ "${file##*.}"x = "so"x ]
            then
            local path="$1/$file"
            local name=$file
            if [ ! -f $2"/"$name ]
            then
                cp -f ${path} "$2/${name}"
            fi
           fi
       fi
   done
}

function release_framework() {
    copy_framework() {
        local framework_path="$1"
        local out_lib="$2"
        local out_dag="$3"
        local out_conf="$4"

        copy_lib_from_dir "${TOP_DIR}/bazel-bin/${framework_path}" "${out_lib}"
        cp -rf ${TOP_DIR}/${framework_path}/dag/* "${out_dag}"
        cp -rf ${TOP_DIR}/${framework_path}/conf/* "${out_conf}"
    }
    local perception_camera_framework_path="air_service/framework/perception-camera"
    copy_framework "${perception_camera_framework_path}" "${OUT_LIB}" "${OUT_DAG}" "${OUT_CONF}"

    local perception_fusion_framework_path="air_service/framework/perception-fusion"
    copy_framework "${perception_fusion_framework_path}" "${OUT_LIB}" "${OUT_DAG}" "${OUT_CONF}"

    local perception_lidar_framework_path="air_service/framework/perception-lidar"
    copy_framework "${perception_lidar_framework_path}" "${OUT_LIB}" "${OUT_DAG}" "${OUT_CONF}"

    # release launch
    cp -rf ${TOP_DIR}/air_service/launch ${OUT_ROOT}
}

function release_algo_modules() {
    local OUT_MODULES_LIB="${OUT_ROOT}/modules/lib"
    local OUT_MODULES_CONF="${OUT_ROOT}/modules/conf"
    copy_module() {
        local module_path="$1"
        local module_name="$2"
        local out_path="${OUT_MODULES_LIB}/airos_${module_name}"

        [ -d "${out_path}" ] || mkdir -p "${out_path}"
        copy_lib_from_dir "${TOP_DIR}/bazel-bin/${module_path}" "${out_path}"

        cp -rf "${TOP_DIR}/${module_path}/conf" "${out_path}"
        mv "${out_path}/conf/dynamic_module.cfg" "${out_path}"
    }
    copy_module "air_service/modules/perception-camera" "perception_camera"
    copy_module "air_service/modules/perception-fusion" "perception_fusion"
    copy_module "air_service/modules/perception-lidar" "perception_lidar"

    # release model
    local perception_camera_out="${OUT_MODULES_LIB}/airos_perception_camera"
    local perception_camera_path="air_service/modules/perception-camera"
    [ -d "${perception_camera_out}/data" ] || mkdir -p "${perception_camera_out}/data"
    if [ "${ARCH}" = "aarch64" ]; then
        cp -rf "${TOP_DIR}/${perception_camera_path}/algorithm/detector/air_detector/data_orin" "${perception_camera_out}/data/detector/"
    else
        cp -rf "${TOP_DIR}/${perception_camera_path}/algorithm/detector/air_detector/data" "${perception_camera_out}/data/detector/"
    fi
    cp -rf "${TOP_DIR}/${perception_camera_path}/algorithm/tracker/air_tracker/data" "${perception_camera_out}/data/tracker/"

    # viz conf
    local viz_conf=${OUT_CONF}/viz
    [ -d "${viz_conf}" ] || mkdir -p "${viz_conf}"
    cp air_service/modules/perception-visualization/config/*.{ini,pt} ${viz_conf}
}

function release_common_param() {
  for dir in ${SUPPORTED_DEVICE_SERVICE[@]}
  do
    [ -d "${RELEASE_ROOT}/param/device/${dir}" ] || mkdir -p ${RELEASE_ROOT}/param/device/${dir}
  done

  for device in ${SUPPORTED_DEVICE_SERVICE[@]}
  do
    cp -rf ${TOP_DIR}/${device_service_path}/${device}/conf/* ${RELEASE_ROOT}/param/device/${device}
  done
}

function release_services() {
    release_framework
    release_algo_modules
}

function release_middleware() {
    release_middleware_device_service
    release_middleware_device_modules
    release_middleware_protocol
}

function release_app_framework() {
  local app_framework_path="app/framework/pipeline"
  cp -rf ${TOP_DIR}/bazel-bin/${app_framework_path}/airos_app_framework ${OUT_BIN}
  cp -rf ${TOP_DIR}/${app_framework_path}/conf/* ${OUT_CONF}
}

function release_app_modules() {
    local APP_MIDULES=(demo v2x_application)
    local app_modules_path="app/modules"
    for app in ${APP_MIDULES[@]}
    do
        local OUT_APP_LIB="${OUT_ROOT}/app/lib"
        local OUT_APP_CONF="${OUT_ROOT}/app/conf"
        local so_path=${OUT_APP_LIB}/airos_${app}
        local conf_path=${OUT_APP_CONF}/airos_${app}
        [ -d "${so_path}" ] || mkdir -p ${so_path}
        [ -d "${conf_path}" ] || mkdir -p ${conf_path}
        copy_lib_from_dir ${TOP_DIR}/bazel-bin/${app_modules_path}/${app} ${so_path}
        cp -f ${TOP_DIR}/${app_modules_path}/${app}/conf/* ${conf_path}
        cp -f ${TOP_DIR}/${app_modules_path}/${app}/conf/app_lib_cfg.pb ${so_path}
    done
}

function release_appliaction() {
    release_app_framework
    release_app_modules
}

function release_middleware_device_modules() {
    for device in ${SUPPORTED_DEVICE_SERVICE[@]}
    do
        local OUT_DEVICES_LIB="${OUT_ROOT}/device/lib"
        local so_path=${OUT_DEVICES_LIB}/${device}/airos_${device}
        [ -d "${so_path}" ] || mkdir -p ${so_path}
        cp -f ${TOP_DIR}/bazel-bin/${device_modules_path}/${device}/lib*.so ${so_path}
        cp -f ${TOP_DIR}/${device_modules_path}/${device}/conf/* ${so_path}
    done
}

function init_out_path() {
  local out_dirs=(
    "${OUT_ROOT}" 
    "${OUT_DAG}" 
    "${OUT_CONF}" 
    "${OUT_LIB}" 
    "${OUT_BIN}" 
  )
  mkdir -p "${out_dirs[@]}"
}


function build_all() {
    #    bazel build -- //... -//package/...
    bazel build //...
    echo "build all done!"
}

function build_framework() {
    local opts="--define cuda=no --copt=-DPERCEPTION_CPU_ONLY"
    declare -a projects=(
        "//air_service/framework/perception-camera/pipeline:libcamera_detection_component.so"
        "//air_service/framework/perception-fusion/pipeline:libmulti_sensor_fusion_component.so"
        "//air_service/framework/perception-lidar/pipeline:liblidar_detection_component.so"
        "//app/framework/..."
        "//middleware/device_service/framework/..."
        "//base/..."
    )

    for project in ${projects[@]}; do
        bazel build $project $opts
    done
    echo "build framework done!"
}

function install_framework() {
    init_out_path
    release_common_param
    release_framework
    release_middleware_device_service
    release_middleware_protocol
    release_app_framework
    cp -rf setup.bash ${OUT_ROOT}
    echo "install all done!"
}

function install_all() {
    init_out_path
    release_common_param
    release_services
    release_middleware
    release_appliaction
    cp -rf setup.bash ${OUT_ROOT}
    echo "install all done!"
}

function release_out {
    local out_dir=${OUT_3RD}
    [ -d "${out_dir}" ] || mkdir -p ${out_dir}

    for lib in $(ls /opt)
    do
        if [ -e "/opt/${lib}/lib" ]; then
            find /opt/${lib}/lib/ -name "lib*.so*" | grep -v stubs | xargs -I {} cp -f {} ${out_dir}/
        fi
    done

    local bazel_cache_lib_path="bazel-bin/_solib_*/"

    find ${bazel_cache_lib_path} -name "lib*.so*" | xargs -I {} cp -f {} ${out_dir}/
    echo "release done."
}

function clean_func() {
    bazel clean
    if [ -d $OUT_ROOT ]; then
        rm -rf $OUT_ROOT
    fi
    echo "clean done!"
}

function run_ut() {
    pushd ${TOP_DIR} > /dev/null
        source setup.bash
    popd

    run_base_device_connect_ut

    echo "ut done!"
}

function main() {
    if [ "$#" -eq 0 ]; then # $#参数个数为0，退出
        exit 0;
    fi

    env_setup

    local cmd="$1"
    shift
    case "${cmd}" in
        build)
            build_all
            install_all
            ;;
        test)
            build_all
            install_all
            release_out
            run_ut
            ;;
        clean)
            clean_func
            ;;
        release)
            build_all
            install_all
            release_out
            ;;
        framework)
            build_framework
            install_framework
            release_out
            ;;
        *)
            ;;
    esac
}

main "$@" # 传入$@所有参数，执行main
