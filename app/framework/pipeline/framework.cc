#include "app/framework/proto/airos_app_config.pb.h"

#include "app/framework/pipeline/app_loader.h"
#include "app/framework/pipeline/send_controller.h"
#include "base/io/protobuf_util.h"
#include "middleware/runtime/src/air_middleware_node.h"

int main(int argc, char* argv[]) {
  std::string app_path = "app/lib";
  if (argc < 2) {
    // ./bin/framework conf/app/airos_v2x_app.pb
    std::cout << "usage: ./bin/framework <conf path>" << std::endl;
    return -1;
  }
  airos::app::AppliactionConfig cfg;
  if (airos::base::ParseProtobufFromFile<airos::app::AppliactionConfig>(
          std::string(argv[1]), &cfg) == false) {
    LOG_WARN << argv[1] << " parse failed!";
    return false;
  }

  airos::middleware::AirRuntimeInit(argv[0]);

  auto send_ctrl = std::make_shared<::airos::app::SendController>();
  auto cb        = std::bind(
      &airos::app::SendController::AppSendCallback, send_ctrl,
      std::placeholders::_1);
  auto loader = std::make_shared<::airos::app::AppLoader>();
  if (!loader->LoadAppliaction(app_path, cfg, cb)) {
    std::cout << "app loading failed" << std::endl;
    return -1;
  }
  pause();
  return 0;
}
