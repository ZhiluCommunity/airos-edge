#! /usr/bin/env bash
OUT_DIR="/home/airos/os"

# 3rd
for lib in $(ls "/opt/local")
do
    if [ -e "/opt/local/${lib}" ]; then
        export LD_LIBRARY_PATH=/opt/local/${lib}/lib:$LD_LIBRARY_PATH
    fi
done

# cyberRT
source /opt/local/cyber-rt/setup.bash
export PYTHONPATH="/usr/local/lib/python2.7/dist-packages/protobuf-3.14.0-py2.7.egg:${PYTHONPATH}"

export GLOG_log_dir="/home/airos/log"
mkdir -p ${GLOG_log_dir}

export LD_LIBRARY_PATH=${OUT_DIR}/3rd:$LD_LIBRARY_PATH

# airservice
export LD_LIBRARY_PATH=${OUT_DIR}/lib:$LD_LIBRARY_PATH

# package
pkg_dir=(modules app)
for pkg in "${pkg_dir[@]}"; do
  for sub_dir in "${pkg}"/lib/*; do
    if [ -d "$sub_dir" ]; then
      export LD_LIBRARY_PATH="$sub_dir:$LD_LIBRARY_PATH"
      if [ -d "${sub_dir}/lib" ]; then
        export LD_LIBRARY_PATH="${sub_dir}/lib:$LD_LIBRARY_PATH"
      fi
    fi
  done
done

for all_device in ${OUT_DIR}/device/lib/*; do
  for device in "${all_device[@]}"; do
    for pkg in ${device}/*; do
      export LD_LIBRARY_PATH="$pkg:$LD_LIBRARY_PATH"
      if [ -d "${pkg}/lib" ]; then
        export LD_LIBRARY_PATH="${pkg}/lib:$LD_LIBRARY_PATH"
      fi
    done
  done
done

export PATH=${OUT_DIR}/launch/:$PATH

# env
export PARAM_DIR="/home/airos/param/"