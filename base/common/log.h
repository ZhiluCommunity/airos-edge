/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#ifndef CONFIG_USE_CYBER_LOG

#include "glog/logging.h"

#define LOG_DEBUG DLOG(INFO)
#define LOG_INFO LOG(INFO)
#define LOG_WARN LOG(WARNING)
#define LOG_ERROR LOG(ERROR)
#define LOG_FATAL LOG(FATAL)
#define LOG_V(log_severity) VLOG(log_severity)
#define LOG_DATA_RECYCLE LOG(INFO)
// LOG_IF
#define LOG_INFO_IF(cond) LOG_IF(INFO, cond)
#define LOG_ERROR_IF(cond) LOG_IF(ERROR, cond)

// LOG_EVERY_N
#define LOG_INFO_EVERY(freq) LOG_EVERY_N(INFO, freq)
#define LOG_WARN_EVERY(freq) LOG_EVERY_N(WARNING, freq)
#define LOG_ERROR_EVERY(freq) LOG_EVERY_N(ERROR, freq)

#define GLOG_TIMESTAMP(timestamp) std::to_string(timestamp)

#else

#include "cyber/common/log.h"
#define LOG_DEBUG ADEBUG
#define LOG_INFO AINFO
#define LOG_WARN AWARN
#define LOG_ERROR AERROR
#define LOG_FATAL AFATAL

#endif
